pragma solidity ^0.4.19;

contract Registration
{
    struct Voter {
        string name;
        uint tokenId;
        bool isVoted;
        string ipfsHash;
    }

    struct Candidate {
        Voter voter;
        uint votes;
    }

    address[] public voters;
    mapping (address => Voter) voterMap;
    mapping (address => Candidate) candidateMap;

    function validVoter(address voter) public view returns (bool) {
        for(uint i = 0; i < voters.length; i++) {
            if (voters[i] == voter) {
                return true;
            }
        }
        return false;
    }

    function voterLength() public view returns (uint) {
        return voters.length;
    }
    
    function getVoter(address voterAddress) public view returns (string, uint, bool, string) {
        Voter memory voter = voterMap[voterAddress];
        return (voter.name, voter.tokenId, voter.isVoted, voter.ipfsHash);
    }

    function getCandidate(address candidateAddress) public view returns (string, uint, string) {
        Candidate memory candidate = candidateMap[candidateAddress];
        return (candidate.voter.name, candidate.votes, candidate.voter.ipfsHash);
    }

    function iterateCandidate(uint index) public view returns (string, uint, address, string) {
        Candidate memory candidate = candidateMap[voters[index]];
        return (candidate.voter.name, candidate.votes, voters[index], candidate.voter.ipfsHash);
    }

    function toString(address x) public view returns (string) {
        bytes memory b = new bytes(20);
        for (uint i = 0; i < 20; i++)
            b[i] = byte(uint8(uint(x) / (2**(8*(19 - i)))));
        return string(b);
    }

    function regVoter(string name, string ipfsHash) public {
        require(!validVoter(msg.sender),"Already Registered");
        voters.push(msg.sender);
        voterMap[msg.sender] = Voter(name, 0, false, ipfsHash);
    }

    function regCandidate() public {
        require(validVoter(msg.sender),"Voter not registered");
        candidateMap[msg.sender] = Candidate(voterMap[msg.sender], 0);
    }
}