// Import the page's CSS. Webpack will know what to do with it.
import '../styles/app.css'

// Import libraries we need.
import { default as Web3 } from 'web3'
import { default as contract } from 'truffle-contract'

import voteArtifact from '../../build/contracts/Vote.json'
import $ from 'jquery'
import { upload } from './fileread'

let accounts
let Vote, isVoted

const App = {
  start: function () {
    console.log(window.location.pathname)
    Vote = contract(voteArtifact)
    Vote.setProvider(web3.currentProvider)
    this.getVoterInfo()
    // Get the initial account balance so it can be displayed.
    web3.eth.getAccounts(function (err, accs) {
      if (err != null) {
        alert('There was an error fetching your accounts.')
        return
      }

      if (accs.length === 0) {
        alert("Couldn't get any accounts! Make sure your Ethereum client is configured correctly.")
        return
      }

      accounts = accs
    })
  },
  register: function () {
    event.preventDefault()
    let name = $('#name').val()
    let input = $('#fileinput').prop('files')[0]
    let hash
    Vote.deployed().then((contractInstance) => {
      upload(input, input.name)
        .then((res) => {
          hash = res
          console.log(hash)
          return contractInstance.regVoter(name, hash, { gas: 340000, from: web3.eth.accounts[0] })
        }).then((res) => {
          console.log(res)
          // console.log(window.web3.toUtf8(res.tx))
          console.log('Registered voter')
          return contractInstance.add({ gas: 140000, from: web3.eth.accounts[0] })
        }).then((res) => {
          console.log('Added token')
          window.location.href = 'http://localhost:8081/index.html'
        }).catch((err) => {
          console.log(err)
          alert('Registered user')
          window.location.href = 'http://localhost:8081/index.html'
        })
    })
  },
  registerAsCandidate: function (event) {
    event.preventDefault()
    Vote.deployed().then((contractInstance) => {
      contractInstance.regCandidate({ gas: 140000, from: web3.eth.accounts[0] })
        .then((res) => {
          console.log('Registered as candidate')
        }).catch((err) => {
          console.log(err)
        })
    })
  },
  checkVoterRegistration: function () {
    Vote.deployed().then((contractInstance) => {
      contractInstance.validVoter.call(web3.eth.accounts[0])
        .then((v) => {
          console.log(v)
        }).catch((err) => {
          console.log(err)
        })
    })
  },

  votingResults: function () {
    let length
    let candidates = []
    Vote.deployed().then((contractInstance) => {
      contractInstance.voterLength.call()
        .then((res) => {
          length = res.toNumber()
          for (let i = 0; i < length; i++) {
            contractInstance.iterateCandidate.call(i)
              .then((res) => {
                if (res[0] !== '') {
                  candidates.push({
                    name: res[0],
                    votes: res[1].toNumber()
                  })
                }
                if (i === length - 1) {
                  window.location.href = 'http://localhost:8081/results.html?jsonObjects=' + encodeURIComponent(JSON.stringify(candidates))
                }
              })
          }
        })
    })
  },

  getVoterInfo: function () {
    Vote.deployed().then((contractInstance) => {
      contractInstance.getVoter.call(web3.eth.accounts[0])
        .then((v) => {
          isVoted = v[2]
        })
    })
  },

  voteForCandidate: function (address) {
    Vote.deployed().then((contractInstance) => {
      contractInstance.transfer(address, { gas: 140000, from: web3.eth.accounts[0] })
        .then((res) => {
          window.web3.eth.getTransaction(res.tx, (err, res) => {
            if (err) {
              console.log(err)
            }
            console.log(res)
            console.log(window.web3.toAscii(res.input))
          })
          console.log('Successfully voted')
        }).catch((err) => {
          console.log(err)
        })
    })
  },
  showCandidates: function () {
    let length
    let candidateList = []
    Vote.deployed().then((contractInstance) => {
      contractInstance.voterLength.call()
        .then((v) => {
          length = v.toNumber()
          $('#banner2').html('')
          for (let i = 0; i < length; i++) {
            contractInstance.iterateCandidate.call(i)
              .then((res) => {
                console.log(res)
                if (res[0] !== '') {
                  $('#banner2').append(`     <div class="col-sm-4" style="border-radius: 10px;">
                  <div class="cards bg-info" id="c3" style="border-radius: 10px;">
                <div class="row">
                  <div class="col-sm-12 bgImage">

                    <center><img class="img-responsive" style="width:100%;border-radius:10px 10px 0px 0px;
                    width:400px; height:360px;" alt="Image"  src="${'http://localhost:8080/ipfs/' + res[3]}"></center>
                  </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                      <input type='submit' name='button' class="btn btn-primary submit-button" onclick='App.voteForCandidate("${res[2]}")' id="aap" value='Vote ${res[0]}' 
                      style="margin-left:100px;margin-top: 20px;">
                    </div>
                </div>
                <div class="row">
                      <div class="col-sm-12">
                    <br>
                      </div>
                  </div>
                  </div>
              
        </div>`)
                  console.log('Isvoted' + isVoted)
                  $('.submit-button').prop('disabled', isVoted)
                }
              })
          }
          return Promise.resolve(candidateList)
        }).catch((err) => {
          console.log(err)
        })
    })
  }
}

window.App = App

window.addEventListener('load', function () {
  // Checking if Web3 has been injected by the browser (Mist/MetaMask)
  if (typeof web3 !== 'undefined') {
    console.warn(
      'Using web3 detected from external source.' +
      ' If you find that your accounts don\'t appear or you have 0 MetaCoin,' +
      ' ensure you\'ve configured that source properly.' +
      ' If using MetaMask, see the following link.' +
      ' Feel free to delete this warning. :)' +
      ' http://truffleframework.com/tutorials/truffle-and-metamask'
    )
    // Use Mist/MetaMask's provider
    window.web3 = new Web3(web3.currentProvider)
  } else {
    console.warn(
      'No web3 detected. Falling back to http://127.0.0.1:9545.' +
      ' You should remove this fallback when you deploy live, as it\'s inherently insecure.' +
      ' Consider switching to Metamask for development.' +
      ' More info here: http://truffleframework.com/tutorials/truffle-and-metamask'
    )
    // fallback - use your fallback strategy (local node / hosted node + in-dapp id mgmt / fail)
    window.web3 = new Web3(new Web3.providers.HttpProvider('http://127.0.0.1:9545'))
  }

  App.start()
})
