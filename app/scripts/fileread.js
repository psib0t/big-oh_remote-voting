let ipfsApi = require('ipfs-api')

let ipfs = ipfsApi('localhost', 5001, { protocol: 'http' })

export function upload (file, filename) {
  var reader = new FileReader()
  reader.readAsArrayBuffer(file)
  return new Promise((resolve, reject) => {
    reader.onload = function () {
      console.log('Reader')
      console.log(reader)
      ipfs.files.add([Buffer.from(reader.result)])
        .then((res) => {
          resolve(res[0].hash)
        }).catch((err) => {
          reject(err)
        })
    }
  })
}
