let fs = require('fs')
const InputDataDecoder = require('ethereum-input-data-decoder')

const abi = JSON.parse(fs.readFileSync(`${__dirname}/build/contracts/Registration.json`))
const decoder = new InputDataDecoder(abi.abi)

console.log(decoder.decodeData('0x1a69523000000000000000000000000029aac1b22b89735ef9853cb21d9d2541fb44a4ce'))
